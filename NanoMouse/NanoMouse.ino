#include <Servo.h>
#include "Nanomousemoters.h"
#include "NanoMouseSensors.h"

const byte ledPin = 13;
const byte buttonPin = 9;

NanoMouseMotors motors;

//<leftEmitter,leftDetector,frontEmitter...
NanoMouseSensors<4,A7,3,A6,2,A5> sensors;

void setup()
{
  motors.attach(6, 5);

  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);\
  
  sensors.configure();
  
  Serial.begin(9600);

  while (digitalRead(buttonPin))
  {
  }


}

void loop()
{
  sensors.sense();
  digitalWrite(ledPin, HIGH);
  sensors.view();
}